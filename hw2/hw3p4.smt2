(define-fun x1_f ((e1 Real) (e2 Real)) Real
  (+ (+ 3 e1) (* 2 e2))
)
(define-fun y1_f ((e1 Real) (e2 Real)) Real
  (+ e1 e2)
)
(define-fun x2_f ((e1 Real) (e2 Real)) Real
  (- (+ 1 e2) (* 2 e1))
)
(define-fun y2_f ((e1 Real) (e2 Real)) Real
  (+ e1 e2)
)
(define-fun x3_f ((e1 Real) (e2 Real) (e3 Real)) Real
  (+ (+ 2 e2) (* 3 e3))
)
(define-fun y3_f ((e1 Real) (e2 Real) (e3 Real)) Real
  (+ e1 e2)
)
(declare-const e1 Real)
(assert (and (>= e1 -1) (<= e1 1)) )
(declare-const e2 Real)
(assert (and (>= e2 -1) (<= e2 1)) )
(declare-const e31 Real)
(assert (and (>= e31 -1) (<= e31 1)) )
(declare-const e32 Real)
(assert (and (>= e32 -1) (<= e32 1)) )
(declare-const e33 Real)
(assert (and (>= e33 -1) (<= e33 1)) )

(declare-const x1 Real)
(declare-const y1 Real)
(declare-const x2 Real)
(declare-const y2 Real)


(assert (forall ((x Real) (y Real)) (
  =>
  ( or 
    (and (= x (x2_f e1 e2)) (= y (y2_f e1 e2)))
    (and (= x (x1_f e1 e2)) (= y (y1_f e1 e2)))
  )
  (and (= x (x3_f e31 e32 e33)) (= y (y3_f e31 e32 e33))))))

(check-sat) ; sat
