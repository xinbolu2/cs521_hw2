(declare-const x Int)
(declare-const y Int)
(declare-const z Int)
(declare-const i Int)

(assert (not                         ; negation of soundness property 
(=>  
    (and (= x 1) (= y 2) (= z 3) (= i 0))          ; if input is within given bounds
    (and (<= i 10) (>= i 0))                       ; then output should satisfy post-condition
)))

(check-sat) ; the solver will return unsat
